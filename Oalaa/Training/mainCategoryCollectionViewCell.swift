//
//  mainCategoryCollectionViewCell.swift
//  Oalaa
//
//  Created by Taufiq Ramadhany on 23/05/20.
//  Copyright © 2020 M2-911. All rights reserved.
//

import UIKit

class mainCategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainImageCategory: UIImageView!
    
}
