//
//  TrainingVC.swift
//  Oalaa
//
//  Created by Steven Wijaya on 20/05/20.
//  Copyright © 2020 M2-911. All rights reserved.
//

import UIKit
import AVFoundation
import CoreData


class TrainingVC: UIViewController {

    @IBOutlet weak var trainingDefaultImage: UIImageView!
    @IBOutlet weak var nameOfTrainingImage: UILabel!
    @IBOutlet weak var playbuttonText: UIButton!
    @IBOutlet weak var timerTextField: UITextField!
    
    
    var dataManager: DataManager = DataManager()
    lazy var activeCategory: NSManagedObject = dataManager.getCategory(coreVocab: false, installed: true, index: 0)
    var answerText: String = ""
    var defaultImage = UIImage (named: "dionTraining")
    var defaultCard = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // text to speech part
    @IBAction func playTrainingButton(_ sender: UIButton) {
        let utterance = AVSpeechUtterance(string: answerText)
        utterance.voice = AVSpeechSynthesisVoice(language: "en")
        utterance.rate = 0.5

        let synthesizer = AVSpeechSynthesizer()
        synthesizer.speak(utterance)
        
    }
        
    @IBAction func reloadTrainingImage(_ sender: Any) {
        let fetchRandomSoundcard: NSManagedObject = dataManager.getRandomInstalledSoundcard()
        
            UIView.transition(with: trainingDefaultImage, duration: 0.5, options: .transitionFlipFromLeft, animations: nil, completion: nil)
            
            trainingDefaultImage.image = dataManager.getSoundcardImageFor(soundcard: fetchRandomSoundcard)
            
            nameOfTrainingImage.text = fetchRandomSoundcard.value(forKey: "soundcardName") as? String
            
            let utterance = AVSpeechUtterance(string: fetchRandomSoundcard.value(forKey: "soundcardName") as! String)
            utterance.voice = AVSpeechSynthesisVoice(language: "en")
            utterance.rate = 0.5
            let synthesizer = AVSpeechSynthesizer()
            synthesizer.speak(utterance)
            answerText = "Please say ," + nameOfTrainingImage.text! + ", I'm Listening"
            
            TaskManager.addAction(action: .speak)
            //playbuttonText.setTitle("Next Card", for: .normal)
            playbuttonText.isHidden = true
            timer()
    }
    
    func timer() {
    var timeLeft = 5
    
          Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
                 print("timer fired!")
            
                timeLeft -= 1
            
                self.timerTextField.text = String(timeLeft)
                print(timeLeft)
            
            if(timeLeft==0){
                        timer.invalidate()
                //self.autoCardReload()
                self.playbuttonText.isHidden = false
                    }
           }
    }
    
   
    
    
    func autoCardReload () {
        let fetchRandomSoundcard: NSManagedObject = dataManager.getRandomInstalledSoundcard()
        
            UIView.transition(with: trainingDefaultImage, duration: 0.5, options: .transitionFlipFromLeft, animations: nil, completion: nil)
            
            trainingDefaultImage.image = dataManager.getSoundcardImageFor(soundcard: fetchRandomSoundcard)
            
            nameOfTrainingImage.text = fetchRandomSoundcard.value(forKey: "soundcardName") as? String
            
            let utterance = AVSpeechUtterance(string: fetchRandomSoundcard.value(forKey: "soundcardName") as! String)
            utterance.voice = AVSpeechSynthesisVoice(language: "en")
            utterance.rate = 0.5
            let synthesizer = AVSpeechSynthesizer()
            synthesizer.speak(utterance)
            answerText = "Please say ," + nameOfTrainingImage.text! + ", I'm Listening"
            
            TaskManager.addAction(action: .speak)
            playbuttonText.setTitle("Next Card", for: .normal)
            playbuttonText.isHidden = true
        
    }
    
}
